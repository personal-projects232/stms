import { useParams } from "react-router";
import {Table} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import {v4 as uuid} from 'uuid';

const CourseDetails = (props) => {

    const {id} = useParams();
    const history = useHistory();
    let coursea = {id:uuid(), courseName:"", assessments:[
        {id:uuid(),assessmentType:"", assessmentDescription:"",dueDate:""},
      ]};
    
    const [course,setCourse] = useState(
        coursea
    )
    
    useEffect(()=>{
        props.courseList.forEach((COURSE)=>{
            if(COURSE._id === id){
                coursea = COURSE;
            }
        });
        console.log(coursea);
        setCourse(coursea);
    },[])
    




    const handleClick = async () =>{
        console.log("Course Deleted!");
        const response = await fetch('http://127.0.0.1:3001/admin/courses/' + id, {method:'DELETE'});
        const res = await response.json();
        console.log(res);
        if(res.delete == true){
            console.log(res.results);
            window.location.reload(false);
            window.location.href = '/'
        }
        else{
            console.log(res.results);
        }
    }

    const handleChangeInput=(id,e)=>{
      const copyCourse = {...course}
      const copyAssessments = (copyCourse.assessments).map((i)=>{
          if(i._id === id){
            i[e.target.name]= e.target.value
          }
          return i
      })

      copyCourse.assessments=copyAssessments;
      setCourse(copyCourse)
    }

    const handleDelete=(id)=>{
        const copyCourse = {...course}
        const copyAssessments = (copyCourse.assessments).filter((item)=>item._id !==id)
        copyCourse.assessments = copyAssessments;
        setCourse(copyCourse);
    }

    return (  
        <div className="course-details">
            <h2>{course.courseName}</h2>

        <Table striped bordered >
            <thead>
                <tr>
                <th>#</th>
                <th>Assessment Type</th>
                <th>Assessment Description</th>
                <th>Date</th>
                <th>Option</th>
                </tr>
            </thead>
            <tbody>
                {
                    (course.assessments).map((item,index) => (
                    <tr id={item._id}>
                        <td>{index}</td>
                        <td>
                            <input
                                 type="text"
                                 value={item.assessmentName}
                                 name="assessmentType"
                                 onChange={(e)=>handleChangeInput(item._id,e)}
                            />
                        </td>
                        <td>
                            <input
                                    type="text"
                                    value={item.assessmentDescription}
                                    name="assessmentDescription"
                                    onChange={(e)=>handleChangeInput(item._id,e)}
                                />
                         </td>
                        <td>
                            <input
                                type="text"
                                value={item.assessmentDueDate}
                                name="dueDate"
                                onChange={(e)=>handleChangeInput(item._id,e)}
                                />
                        
                       </td>
                        <td>
                            <button onClick={e=>handleDelete(item._id,e)}>Delete</button>
                        </td>
                    </tr>
                    ))}
            </tbody>
            </Table>

            <button onClick={handleClick} className="delete-course">
                Delete Course
            </button>
        </div>
    );
}

 
export default CourseDetails;


          