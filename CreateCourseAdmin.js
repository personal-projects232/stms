import { useState } from "react";
import { useHistory } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const CreateCourseAdmin = (props) => {

    const history = useHistory();
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [requiredHours, setRequiredHours] = useState("");
    const [courseName,setCourseName]= useState("");
    const [assessmentDescription,setAssessmentDescription]= useState("");
    const [assessmentType,setAssessmentType]= useState("Assignment");
    const setHandleAddOption = (e)=>{
        console.log(e.target.value);
        if(e.target.value == 'assignment'){
            document.getElementById('courseNameInput').style.display = 'none';
            document.getElementById('courseNameSelector').style.display = 'block';
        
            document.getElementById('assessmentType').style.display = 'block';
            document.getElementById('assessmentDescription').style.display = 'block';
            document.getElementById('assessmentTypeLabel').style.display = 'block';
            document.getElementById('assessmentDescriptionLabel').style.display = 'block';
            document.getElementById('addAssessmentButton').style.display = 'inline';
            document.getElementById('minimumStudyHours').style.display = 'none';
            document.getElementById('minimumStudyHoursLabel').style.display = 'none';
            document.getElementById('addCourseButton').style.display = 'none';
            document.getElementById('startDateLabel').innerHTML = 'Due Date';
            document.getElementById('startDateLabel').style.display = 'block';
            document.getElementById('startDate').style.display = 'block';
            document.getElementById('endDateLabel').style.display = 'none';
            document.getElementById('endDate').style.display = 'none';
       
        }
        else{   
            console.log(document.getElementById('courseNameInput').style.display);
            document.getElementById('assessmentType').style.display = 'none';
            document.getElementById('assessmentDescription').style.display = 'none';
            document.getElementById('assessmentTypeLabel').style.display = 'none';
            document.getElementById('assessmentDescriptionLabel').style.display = 'none';
            document.getElementById('addAssessmentButton').style.display = 'none';
            document.getElementById('minimumStudyHours').style.display = 'block';
            document.getElementById('minimumStudyHoursLabel').style.display = 'block';
            document.getElementById('addCourseButton').style.display = 'inline';
            document.getElementById('courseNameSelector').style.display = 'none';
            document.getElementById('courseNameInput').style.display = 'block';
            document.getElementById('startDateLabel').innerHTML = 'Start Date';
            document.getElementById('startDateLabel').style.display = 'block';
            document.getElementById('startDate').style.display = 'block';
            document.getElementById('endDateLabel').style.display = 'block';
            document.getElementById('endDate').style.display = 'block';
        }
    }
    const handleSubmitAssessment=(e)=>{
        // prevent form from refreshing after pressing assessment button
        e.preventDefault();
        // reset Form
        setStartDate("");
        setAssessmentDescription("");
        setAssessmentType("Assignment");
    }

    const handleAddAssessmentButton = async (e) =>{
        let data = [
            {COURSE:courseName},
            {
            ASSESSMENTTYPE: assessmentType,
            ASSESSMENTDESCRIPTION: assessmentDescription,
            ASSESSMENTDUEDATE: startDate    
        }];
        console.log(data);
        /*
        [
            { "course":"MAM2000W"}
            ,
            {
            "assessmentType": "assessmentType",
            "assessmentDescription": "assessmentDescription",
            "assessmentDueDate": "startDate"    
        }]

        */
        let post = {
            method: 'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify(data)
        }
        let response = await fetch('http://127.0.0.1:3001/admin/:userId/new_assessment',post);
        let finalRespone = await response.json();

        if(finalRespone.success == true){
            window.location.reload(false);
            window.location.href = '.';
        }
        else{
            console.log('failed');
        }
    }

    const handleAddCourseButton= async (e)=>{
        const data = {COURSENAME:courseName,
        REQUIREDHOURS: requiredHours,
        STARTDATE:startDate,
        ENDDATE:endDate,
        ASSESSMENTS:[]
        }     
        let post = {
            method: 'POST',
            headers:{'Content-Type':'application/json'},
            body: JSON.stringify(data)
        }
        let response = await fetch('http://127.0.0.1:3001/admin/12/new_course',post);
        let finalResponse = await response.json();
        if(finalResponse.success == true){
            window.location.reload(false);
            window.location.href = '.';
        }
        else{
            console.log(response.results);
        }
     
    }
    let availableCourse = [];
    props.courseList.forEach((course)=>{
        availableCourse.push(<option value={ course.courseName }>{course.courseName}</option>);
    });

    return ( 
        <div className="create">
            <h2>Add A New Course</h2>

            <div className="course-name-label">
            <label>Course/Assignment</label>
                   <select
                    onChange={(e)=>{
                        setHandleAddOption(e)}
                        } required>
                        <option label=" "></option>
                        <option value="course">Course</option>
                        <option value="assignment">Assignment</option>
                    </select>
            </div>

            <div className="assessment">
                <form onSubmit={handleSubmitAssessment} id='form'>
                <label>Course Name  </label>
                <input id="courseNameInput" 
                type="text"  
                value={courseName}
                pattern='[A-Z]{3}[0-9]{3}'
                onChange={(e)=>{
                    setCourseName(e.target.value)
                } 
            }
             />
                <select id="courseNameSelector" onChange={(e)=>setCourseName(e.target.value)}>
                        <option label=" "></option>
                       {availableCourse}
                    </select>

                <label id='assessmentTypeLabel'> Assessment Type</label>
                   <select id='assessmentType'
                    value={assessmentType}
                    onChange={(e)=>{setAssessmentType(e.target.value)}}>
                        <option value="TheoryTest">Theory Test</option>
                        <option value="PracTest">Practical Test</option>
                        <option value="Assignment">Assignment</option>
                    </select>                
                    <label id='assessmentDescriptionLabel'>Assessment Description </label>
                    <input id="assessmentDescription" type="text"
                    value={assessmentDescription}
                    onChange={(e)=>{setAssessmentDescription(e.target.value)}}
                     />
                    <label id='minimumStudyHoursLabel'>Minimum Study Hours Per week </label>
                    <input id="minimumStudyHours" type="text" value={requiredHours} onChange={(e)=>setRequiredHours(e.target.value)}
                     />

                    <label id="startDateLabel" >Start Date</label>
                    <DatePicker id="startDate" 
                    value={startDate}
                    selected={startDate} 
                    dateFormat="dd/MM/yyyy" 
                    placeholderText="Select a date"
                    minDate={new Date()} 
                    onChange={(date) => setStartDate(date)}/>
                    
                    
                    <label id="endDateLabel">End Date</label>
                    <DatePicker id="endDate" 
                    value={endDate}
                    selected={endDate} 
                    dateFormat="dd/MM/yyyy" 
                    placeholderText="Select a date"
                    minDate={new Date()} 
                    onChange={(date) => setEndDate(date)} />
                
                    <button id="addAssessmentButton" onClick={handleAddAssessmentButton} > Add Assessment</button> 
                </form>
                <button id="addCourseButton"
                     onClick={
                         handleAddCourseButton
                        }
                     className="add-course-button"
                     disabled={!courseName}
                     >Add Course</button> 
             </div>

             <div className="preview">
                 <h2>Preview</h2>
                 <p>Course Name: {courseName}</p>
                 <p>Assessment Description: {assessmentDescription}</p>
                 <p>Assessment Type: {assessmentType}</p>
                 <p>Date: {startDate.toString()}</p> 
             </div>
         </div>
     );
    
}
 
export default CreateCourseAdmin;
