import { useHistory } from "react-router-dom";

const Notification = () => {
   const history = useHistory();

    const handleSendButton=()=>{
      history.push('/');
    }

    return ( 
      <div className="notifications">
        <h2>Send A Notification</h2>
        <form onSubmit={handleSendButton}>
         <label> Course List</label>
         <select>
            <option value="TheoryTest">CSC3003S</option>
            <option value="PracTest">EEE3055S</option>
            <option value="Assignment">MAM3000W</option>
          </select>

          <label>Message</label>
          <textarea name="" id="" cols="30" rows="5"></textarea>
          <button>Send</button>
        </form>
      </div>
       

     );
}
 
export default Notification;