import React, { useState } from 'react';
import {v4 as uuid} from 'uuid';
import { Link } from "react-router-dom";


class Home extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="course-list">
            <h2 className="all-courses">All Courses!</h2>
            {this.props.courses.map((course)=>(
                <div className="course-preview" key={course._id}>
                    <Link style={{ textDecoration: 'none' }} to={`/courses/${course._id}`}>
                        <h2> {course.courseName}</h2>
                        <p>Number Of Assessments: {(course.assessments).length}</p>
                        </Link>
                    
                </div>
            ))
        }</div>
    
        );
    }
}
 
export default Home;
//backend staff
// useEffect(() => {
//     const vulaURL="https://vula.uct.ac.za/portal/directtool/70c117bf-6667-4c39-82af-d3d69a311823/";

//     async function fetchData() {
//         const response = await fetch(vulaURL,{
//             Method: "POST",
//             // mode: 'cors',
         
//         });
//         const data = await response.json();
//         console.log(data);
//     }
//     fetchData();
//   }, []); 