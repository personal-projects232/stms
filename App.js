import Home from './Home';
import {BrowserRouter as Router,Route, Switch} from 'react-router-dom';
import './App.css';
import NavbarAdmin from './NavbarAdmin';
import CreateCourseAdmin from './CreateCourseAdmin';
import Notification from './Notification';
import CourseDetails from './CourseDetails';
import React from 'react';


class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      courses: []
    }
  } 
  async componentDidMount(){
    const response = await fetch('http://127.0.0.1:3001/admin/id/courses');
    const data = await response.json();
    this.setState({courses:data});
}
  render(){
  return (
    <Router>
      <div className="App">
        <NavbarAdmin/>
          <Switch>
            <Route exact path="/">
              <Home courses={this.state.courses } />
            </Route>
            <Route path="/create">
              <CreateCourseAdmin  courseList ={ this.state.courses }/>
            </Route>
            <Route path="/notification">
              <Notification/>
            </Route>
            <Route path="/courses/:id">
              <CourseDetails courseList={this.state.courses} />
            </Route>
            <Route path="*"></Route>
          </Switch>
     </div>
    </Router>);
  }
}

export default App;

// <i class="fas fa-plus-circle"></i>
// <i class="fas fa-home"></i>
// <i class="fas fa-bullhorn"></i>