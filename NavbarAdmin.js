import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle,faBullhorn ,faHome, faUserCog} from '@fortawesome/free-solid-svg-icons';

const NavbarAdmin = () => {
    const admin= "ABCXYZ001";
    return (  
        <div className="navbar-admin">
             <h1><FontAwesomeIcon icon={faUserCog} size={"1x"} />Admin/ {admin}</h1>
            <div className="links">
                <Link to="/"><FontAwesomeIcon icon={faHome} size={"2x"} /></Link>
                <Link to="/create"><FontAwesomeIcon icon={faPlusCircle} size={"2x"} /></Link>
                <Link to="/notification"><FontAwesomeIcon icon={faBullhorn} size={"2x"}/></Link>
            </div>
            
        </div>
    );
}
 
export default NavbarAdmin;